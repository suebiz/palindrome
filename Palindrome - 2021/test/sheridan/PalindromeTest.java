package sheridan;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class PalindromeTest {

	@Test
	public void testIsPalindromeRegular( ) {
		assertEquals(true, Palindrome.isPalindrome("kayak"));
	}

	@Test
	public void testIsPalindromeException( ) {
		assertEquals(false, Palindrome.isPalindrome("notapalindronme"));
	}
	@Test
	public void testIsPalindromeBoundaryIn( ) {
		assertEquals(true, Palindrome.isPalindrome("amanaplanacanalpanama"));
	}
	@Test
	public void testIsPalindromeBoundaryOut( ) {
		assertEquals(false, Palindrome.isPalindrome("noPalindrome"));
	}	
	
}
